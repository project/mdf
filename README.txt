 INTRODUCTION
 ------------

 The Merge Duplicate Files (MDF) module extend filehash module functionally and allow to reuse uploaded files
 automatically if user upload the existed file. Module compare files by hash and prevent duplicate files, replace it
 with exists file.

* For a full description of the module, visit the project page:
   https://www.drupal.org/project/mdf

* To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/mdf


REQUIREMENTS
------------

This module requires the following modules:

* Filehash (https://www.drupal.org/project/filehash)


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
    https://www.drupal.org/docs/7/extend/installing-modules
    for further information.


CONFIGURATION
-------------

* Configure the user permissions in Administration » People » Permissions:
  - administer mdf module
    Allow permitted users to access and change MDF module configuration.

* Set allowed hash algorithms on filehash module settings page.
  Goto admin/config/media/filehash page and enable one opr more algorithms.
  Uncheck "Disallow duplicate files" if checked.

* Generate hashes for exists files.
  Goto admin/config/media/filehash/bulk
  and click "Bulk generate file hashes" button.

* Goto /admin/config/media/mdf and select the comparison algorithm and the types of entities for which you want to
  perform checks.


MAINTAINERS
-----------

Current maintainers:
 * Gevorg Mkrtchyan (armrus) - https://www.drupal.org/u/armrus

This project has been sponsored by:
 * Drupal Coder, Initlab, LLC

   Specializing in non-trivial custom development projects:
   - headless Drupal + React,
   - ecommerce,
   - enterprise integration,
   - performance tuning,
   - Drupal SEO,
   - Web Services,
   - Drupal based mobile apps,
   - AR/VR.

   https://drupal-coding.com/